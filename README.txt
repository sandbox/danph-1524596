Registration OG
dpi@d.o - http://danielph.in

Integrates Registration with Organic Groups
Copyright (C) 2012 Daniel Phin

## License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

## Features

  * Restrict editing of fields on Registrations. Which are attached to entities
    which are group content.
    
    __Use case__: Add an attendance field to registrations which can only be 
    edited by group administrators.
    
    __Note__: Disabling of registrations to users can be done through
    og_field_access.module. This feature is for restricting fields on 
    registrations only.
  
  * Adds a permission to enable registrations for group content.
  
    Usage: Disables registration for group content, which a user is not a member of. 
  

## Instructions

  1. 